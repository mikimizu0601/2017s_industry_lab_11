package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {
//        super.paintComponent(g);
        int[] x = new int[5];
        x[0] = left+0*size;
        x[1] = left+5*size;
        x[2] = left+size*10;
        x[3] = left+size*10;
        x[4] = left+size*0;
        int[] y = new int[5];
        y[0] = top+5*size;
        y[1] = top+size*0;
        y[2] = top+size*5;
        y[3] = top+size*12;
        y[4] = top+size*12;

        g.setColor(Color.pink);
        g.fillPolygon(x,y,5);

        g.setColor(Color.yellow);
        g.fillRect(1*size+left,7*size+top,2*size,2*size);
        g.fillRect(7*size+left,7*size+top,2*size,2*size);

        g.setColor(Color.black);
        g.drawLine(0*size+left,5*size+top,size*10+left,size*5+top);
        g.drawLine(7*size+left,1*size+top,size*7+left,size*2+top);
        g.drawLine(7*size+left,1*size+top,size*8+left,size*1+top);
        g.drawLine(8*size+left,1*size+top,size*8+left,size*3+top);
        g.drawRect(4*size+left,8*size+top,2*size,4*size);
        g.drawLine(1*size+left,8*size+top,3*size+left,8*size+top);
        g.drawLine(2*size+left,7*size+top,size*2+left,size*9+top);
        g.drawLine(7*size+left,8*size+top,size*9+left,size*8+top);
        g.drawLine(8*size+left,7*size+top,size*8+left,size*9+top);

        // TODO Draw a house, as shown in the lab handout.
    }
}