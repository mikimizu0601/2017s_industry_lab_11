package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.DoubleSummaryStatistics;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyButton;
    private JTextField height;
    private JTextField weight;
    private JTextField bmi;
    private JTextField maximum;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.

        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyButton = new JButton("Calculate Healthy Weight");

        height = new JTextField(10);
        weight = new JTextField(10);
        bmi = new JTextField(10);
        maximum = new JTextField(10);
        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        Label heightLabel = new Label("Height in metres:");
        Label weightLabel = new Label("Weight in kilograms:");
        Label bmiLabel = new Label("Your Body Mass Index (BMI) is:");
        Label maximumLabel = new Label("Maximum Healthy Weight for your Height:");
        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(height);
        this.add(weightLabel);
        this.add(weight);
        this.add(calculateBMIButton);
        this.add(bmiLabel);
        this.add(bmi);
        this.add(calculateHealthyButton);
        this.add(maximumLabel);
        this.add(maximum);

        // TODO Add Action Listeners for the JButtons
        this.calculateBMIButton.addActionListener(this);
        this.calculateHealthyButton.addActionListener(this);
    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    @Override
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass a String, which will be displayed in the text box.
        double h = Double.parseDouble(height.getText());
            if(event.getSource() == calculateBMIButton){
                double w = Double.parseDouble(weight.getText());
                double b = roundTo2DecimalPlaces(w / (h * h));
                String z = Double.toString(b);
                bmi.setText(z);

            } else if(event.getSource() == calculateHealthyButton) {
                double m = roundTo2DecimalPlaces(24.9 * h * h);
                String y = Double.toString(m);
                maximum.setText(y);
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}