package ictgradschool.industry.lab11.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;
import javax.swing.Timer;
/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements KeyListener, ActionListener {

    private  Balloon balloon;
    private Direction direction;
    Timer timer = new Timer(1000, this);
    List<Balloon> myBalloons= new ArrayList<>();

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        myBalloons.add(new Balloon(200, 10));
        myBalloons.add(new Balloon(30, 100));
        myBalloons.add(new Balloon(10, 600));
        myBalloons.add(new Balloon(100, 80));
        this.addKeyListener(this);

    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("action performed");
        for(Balloon balloon: myBalloons){
            balloon.move();
        }


        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Balloon balloon: myBalloons) {
            balloon.draw(g);
        }
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }


    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("key press");
        for(Balloon balloon: myBalloons) {

            if (e.getKeyCode() == KeyEvent.VK_UP) {

                direction = direction.Up;
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {

                direction = direction.Down;
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                direction = direction.Left;
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                direction = direction.Right;
            }
            balloon.setDirection(direction);
        }
        if(!timer.isRunning()){
            timer.start();
        }
        if(e.getKeyCode() == KeyEvent.VK_S){
            timer.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}