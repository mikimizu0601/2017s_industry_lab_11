package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.DoubleSummaryStatistics;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    private TextField firstText;
    private TextField secondText;
    private TextField result;
    private JButton add;
    private JButton subtract;
    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        firstText = new TextField(10);
        this.add(firstText);
        secondText = new TextField(10);
        this.add(secondText);
        add = new JButton("Add");
        this.add(add);
        this.add.addActionListener(this);
        subtract = new JButton("Subtract");
        this.add(subtract);
        this.subtract.addActionListener(this);
        JLabel resultLabel = new JLabel("Result:");
        this.add(resultLabel);
        result = new TextField(20);
        this.add(result);
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        double x = Double.parseDouble(firstText.getText());
        double y = Double.parseDouble(secondText.getText());
        if(e.getSource() == add){
            String z = Double.toString(roundTo2DecimalPlaces(x + y));
            result.setText(z);
        } else if(e.getSource() == subtract){
            String z = Double.toString(roundTo2DecimalPlaces(x - y));
            result.setText(z);
        }
    }
}